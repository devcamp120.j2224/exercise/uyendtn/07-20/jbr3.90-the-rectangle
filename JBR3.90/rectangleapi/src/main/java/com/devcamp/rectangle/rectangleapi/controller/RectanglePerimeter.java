package com.devcamp.rectangle.rectangleapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rectangle.rectangleapi.model.Rectangle;

@RestController
public class RectanglePerimeter {
    @CrossOrigin
    @GetMapping("rectangle-perimeter")
    public double getPerimeter() {
        Rectangle rectangle2 = new Rectangle(8, 5);
        return rectangle2.getPerimeter();
}
}