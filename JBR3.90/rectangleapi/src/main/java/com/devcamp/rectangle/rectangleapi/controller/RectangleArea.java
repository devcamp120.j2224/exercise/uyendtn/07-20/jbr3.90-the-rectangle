package com.devcamp.rectangle.rectangleapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rectangle.rectangleapi.model.Rectangle;

@RestController
public class RectangleArea {
    @CrossOrigin
    @GetMapping("rectangle-area")
    public double getArea() {
        
        Rectangle rectangle1 = new Rectangle(5, 2);
        
        return rectangle1.getArea();
    }
}
